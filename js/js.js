(function (window) {
    'use strict';
    // Get DOM element by CSS like selector
    window.$ = function(id){
        return document.querySelector(id);
    }
    
    // item object
    window.Item = (function(title, description, price){
        this.title = title;
        this.description = description;
        this.price = price;
        this.quantity = 0;
        Item.prototype.inQuantity = function(){
            this.quantity++;
        }
        Item.prototype.deQuantity = function(){
            this.quantity--;
        }
    });
    
    // basket object
    window.Basket = (function(){
        var itemList = [];
        return {
            size: function(){
                return itemList.length;
            },
            get: function(index){
                return itemList[index];
            },
            getByTitle: function(title){
                for(var i = 0; i < itemList.length; i++){
                    if(itemList[i].title == title) {
                        return itemList[i];
                    }
                }
                return null;
            },
            add: function(item){
                var isExist = false;
                for(var i = 0; i < itemList.length; i++){
                    if(itemList[i].title == item.title) {
                        itemList[i].inQuantity();
                        isExist = true;
                    }
                }
                if(!isExist){
                    item.inQuantity();
                    itemList.push(item);
                }
            },
            remove: function(item){
                for(var i = 0; i < itemList.length; i++){
                    if(itemList[i].title == item.title) {
                        itemList[i].deQuantity();
                        if(itemList[i].quantity < 1) {
                            itemList.splice(i,1); 
                            break;
                        }
                    }
                }
            }
        };
    });
    
    // Shop object
    window.Shop = (function(){
        var itemList = [];
        return {
            size: function(){
                return itemList.length;
            },
            get: function(index){
                return itemList[index];
            },
            getByTitle: function(title){
                for(var i = 0; i < itemList.length; i++){
                    if(itemList[i].title == title) {
                        return itemList[i];
                    }
                }
                return null;
            },
            add: function(item){
                itemList.push(item);
            },
            remove: function(title){
                for(var i = 0; i < itemList.length; i++){
                    if(itemList[i].title == title) {
                        itemList.splice(i,1);
                        return true;
                    }
                }
                return false;
            }
        };
    });
    
    // Shop init
    window.$.openShop = (function(){
        var shop = new Shop();
        var basket = new Basket();
        var itemDisplay = '';
        var basketDisplay = '';
        
        function refreshBasketList(){
            var total = 0;
            $(basketDisplay).innerHTML = "";
            var t_itemList = document.createElement("TABLE");
            var tr_th = document.createElement("TR");
            var th_title = document.createElement("TH");
            var th_description = document.createElement("TH");
            var th_price = document.createElement("TH");
            var th_quantity = document.createElement("TH");
            var th_remove = document.createElement("TH");

            th_title.appendChild(document.createTextNode("Title"));
            th_description.appendChild(document.createTextNode("Description"));
            th_price.appendChild(document.createTextNode("Price"));
            th_quantity.appendChild(document.createTextNode("Quantity"));
            th_remove.appendChild(document.createTextNode("Remove"));

            tr_th.appendChild(th_title);
            tr_th.appendChild(th_description);
            tr_th.appendChild(th_price);
            tr_th.appendChild(th_quantity);
            tr_th.appendChild(th_remove);
            t_itemList.appendChild(tr_th);

            //alert(basket.size());
            for(var i = 0; i < basket.size(); i++){
                // div of item
                var item = basket.get(i);
                
                total += (item.price * item.quantity);
                
                var tr = document.createElement("TR");
                var td_title = document.createElement("TD");
                var td_description = document.createElement("TD");
                var td_price = document.createElement("TD");
                var td_quantity = document.createElement("TD");
                var btn_remove = document.createElement("BUTTON");

                // assign text into the table
                td_title.appendChild(document.createTextNode(item.title));
                td_description.appendChild(document.createTextNode(item.description));
                td_price.appendChild(document.createTextNode(item.price));
                td_quantity.appendChild(document.createTextNode(item.quantity));
                btn_remove.appendChild(document.createTextNode("Remove"));

                // remove button handler
                var removeListener = function(title) {
                    return function() {
                        basket.remove(basket.getByTitle(title));
                        refreshBasketList();
                    };
                }; 
                btn_remove.addEventListener("click", removeListener(item.title));
                
                // add elements into the table
                tr.appendChild(td_title);
                tr.appendChild(td_description);
                tr.appendChild(td_price);
                tr.appendChild(td_quantity);
                tr.appendChild(btn_remove);
                t_itemList.appendChild(tr);
                
            }
            
            // total
            var tr = document.createElement("TR");
            var td_lb_total = document.createElement("TD");
            var td_total = document.createElement("TD");
            
            td_lb_total.appendChild(document.createTextNode("Total:"));
            td_total.appendChild(document.createTextNode(total));
            td_total.setAttribute("colspan", "4");
            
            tr.appendChild(td_lb_total);
            tr.appendChild(td_total);
            t_itemList.appendChild(tr);
            
            $(basketDisplay).appendChild(t_itemList);
        }
        
        function refreshItemList(){
                var t_itemList = document.createElement("TABLE");
                var tr_th = document.createElement("TR");
                var th_title = document.createElement("TH");
                var th_description = document.createElement("TH");
                var th_price = document.createElement("TH");
                var th_add = document.createElement("TH");
                
                th_title.appendChild(document.createTextNode("Title"));
                th_description.appendChild(document.createTextNode("Description"));
                th_price.appendChild(document.createTextNode("Price"));
                th_add.appendChild(document.createTextNode("Add To Basket"));
                
                tr_th.appendChild(th_title);
                tr_th.appendChild(th_description);
                tr_th.appendChild(th_price);
                tr_th.appendChild(th_add);
                t_itemList.appendChild(tr_th);
                
                // add all item into the container
                for(var i = 0; i < shop.size(); i++){
                    // div of item
                    var item = shop.get(i);
                    
                    var tr = document.createElement("TR");
                    var td_title = document.createElement("TD");
                    var td_description = document.createElement("TD");
                    var td_price = document.createElement("TD");
                    var btn_addToBasket = document.createElement("BUTTON");
                    
                    // assign text into the table
                    td_title.appendChild(document.createTextNode(item.title));
                    td_description.appendChild(document.createTextNode(item.description));
                    td_price.appendChild(document.createTextNode(item.price));
                    btn_addToBasket.appendChild(document.createTextNode("Add"));
                    
                    // add button handler
                    var addListener = function(title) {
                        return function() {
                            basket.add(shop.getByTitle(title));
                            refreshBasketList();
                        };
                    }; 
                    btn_addToBasket.addEventListener("click", addListener(item.title));
                    
                    // add elements into the table
                    tr.appendChild(td_title);
                    tr.appendChild(td_description);
                    tr.appendChild(td_price);
                    tr.appendChild(btn_addToBasket);
                    t_itemList.appendChild(tr);
                }
                $(itemDisplay).appendChild(t_itemList);
        }
        
        return {
            addItem: function(title, description, price){
                var item = new Item(title, description, price);
                shop.add(item);
            },
            removeItem: function(title){
                if(shop.remove(title)){
                    return "Item: "+title+" has been removed.";   
                } else {
                    return "Item: "+title+" is not exist.";
                }
            },
            displayItems: function(container) {
                itemDisplay = container;
                refreshItemList();
            },
            displayBasket: function(container, trigger) {
                basketDisplay = container;
                refreshBasketList();
                $(container).parentNode.style.visibility = "hidden";
                $(trigger).addEventListener("click", function(){
                    if ($(container).parentNode.style.visibility == "hidden"){
                        $(container).parentNode.style.visibility = "visible";
                    } else {
                        $(container).parentNode.style.visibility = "hidden";
                    }
                });
            }
        };
    });
})(window);